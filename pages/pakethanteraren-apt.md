---
title: Pakethanteraren apt
slug: pakethanteraren-apt
---

Det finns olika sätt att installera program i ett Linux-system. Ett smidigt sätt är att använda en _pakethanterare_; i Linux kallas program för  _programpaket_ eller bara _paket_. Det kallas så för att programmet består av en massa filer som paketeras tillsammans. Det vi ska titta på är en pakethanterare som heter _apt_.

## Hitta och installera program

Det finns en lista på internet över vilka program som finns tillgängliga, och var på internet de finns att hämta. En kopia över listan finns i vår dator. Men listan på internet ändras hela tiden allt eftersom det kommer nya versioner av programmen, och därför måste vi uppdatera den lista som finns lokalt på vår dator, till den senaste versionen:

- `apt update`: uppdaterar listan över program till den senaste versionen

Här är några vanliga användningsfall:

- `sudo apt install [paketnamn]`: installera ett nytt programpaket
- `sudo apt remove [paketnamn]`: avinstallera (ta bort) ett programpaket från systemet
- `apt list`: lista alla tillgängliga paket (ger en lååång lista)
- `apt list --installed`: lista alla paket som är installerade i systemet (ger en ganska lång lista)
- `apt search [paketnamn]`: leta efter ett paket
- `apt show [paketnamn]`: visa information om programmet

**Tips:** här kan det vara lämpligt att styra om utskriften till programmet more. Exempel: `apt list --installed | more`

## uppgradera programpaket till senaste versionen
- `sudo apt upgrade [paketnamn]`: uppgradera till den senaste versionen av ett program
- `apt upgrade`: anger man inget paketnamn, så uppgraderas alla programpaket

## Några förslag

- `sudo apt install ne`: Nice Editor: en 
- `sudo apt install tldr`: "To lazy, didn't read", en lite enklare hjälp-program
- `sudo apt install lynx`: En textbaserad webbläsare - prova att surfa på webben i terminalen!

### ... och lite skojiga saker

- `sudo apt install lolcat`: Styr utskriften till lolcat, så blir texten färgglad
- `sudo apt install cowsay`: Låt en ko prata ...
- `sudo apt install figlet`: Gör utskriften stor och snygg.
- `sudo apt install cmatrix`: Få en cool matrix-skärm

